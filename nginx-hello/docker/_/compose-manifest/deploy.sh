#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
D_=$(cd "$SH/.." && pwd)  # D_ aka docker/_ folder

    ln -sfn "$D_/.env"                    "$SH/.env"
    ln -sfn "$D_/image-push.env"          "$SH/image-push.env"
    ln -sfn "$D_/compose/.env.at-runtime" "$SH/.env.at-runtime"

    set -a
        source "$SH/.env"
        source "$SH/image-push.env"
        source "$SH/.env.at-runtime"
    set +a
 
        eval "echo \"$(cat docker-compose.manifest.tpl)\"  > $SH/docker-compose.manifest"

            (cd $SH \
                && docker-compose  -f "$SH/docker-compose.manifest"  down -t0  `# down first` \
                && docker-compose  -f "$SH/docker-compose.manifest"  up -d     `# then up`    \
                && `#              = manifest file to deploy         .  . ` )

                source "$D_/aftermath.sh"
