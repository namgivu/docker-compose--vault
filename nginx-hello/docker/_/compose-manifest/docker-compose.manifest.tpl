version: '3'

services:

  nginx:
    image: $DOCKERHUB_USER/$IMAGE_NAME:latest
    #      =REMOTE_IMAGE_NAME

    ports:
      - $PORT:80

    environment:
      - VAR=$VAR
    #NOTE in case too many envvar, use env file ref. https://stackoverflow.com/q/42149529/248616
    #env_file:
    #  - .env.at-runtime

    restart: unless-stopped

    hostname      : $CONTAINER_NAME
    container_name: $CONTAINER_NAME
