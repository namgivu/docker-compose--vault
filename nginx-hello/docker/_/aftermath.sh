#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
source "$SH/.env"

    # print envvar
    docker exec -it $CONTAINER_NAME bash -c env | grep VAR --color=always

    # view nginx homepage
    curl -sI "localhost:$PORT" | grep '200 OK'
        [ $? == 0 ] && echo 'pass' || echo 'fail'

        echo
        curl "localhost:$PORT"
