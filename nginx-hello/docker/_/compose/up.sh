#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
D_=$(cd "$SH/.." && pwd)  # D_ aka docker/_ folder
AH=$(cd "$SH/../../.." && pwd)

    #                              docker-compose up will auto load this .env
    ln -sfn "$D_/.env"            "$SH/.env"            # load build-time param via .env file - we reuse .env of Dockfile build here
    ln -sfn "$D_/image-push.env"  "$SH/image-push.env"  # load param for docker image push - we reuse D_ file here
    ln -sfn "$AH/.env"            "$SH/.env.at-runtime"  # app's .env file is container runtime envvar
    #       dockerfile            dockercompose

    set -a  # automatically export all variables ON
        source "$SH/.env"             # called as uniform with other env file loading here - should be already auto-loaded by 'docker-compose up'
        source "$SH/image-push.env"   # load $DOCKERHUB_USER
        source "$SH/.env.at-runtime"  # load $VAR
    set +a  # automatically export all variables OFF

        docker-compose  -f "$SH/docker-compose.yml"  up  -d
