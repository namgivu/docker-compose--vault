#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

source "$SH/image-push.env"
    [ -z $DOCKERHUB_PASS ] && (echo 'Param DOCKERHUB_PASS is required'; kill $$; exit 1)
    [ -z $DOCKERHUB_USER ] && (echo 'Param DOCKERHUB_USER is required'; kill $$; exit 1)

    source "$SH/.env"  # load IMAGE_NAME

        echo "$DOCKERHUB_PASS" | docker login  --username $DOCKERHUB_USER  --password-stdin  # login docker hub
        #     !                  .             !          .                !

            [ $? != 0 ] && (echo 'Login failed'; kill $$; exit 1)

                # push image ref. https://stackoverflow.com/a/45554728/248616
                docker tag  $IMAGE_NAME "$DOCKERHUB_USER/$IMAGE_NAME:latest"
                docker push             "$DOCKERHUB_USER/$IMAGE_NAME:latest"
                #      .                =REMOTE_IMAGE_NAME
