#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

sample_usage='
REGISTRY_PROVIDER=dockerhub ./image-push.sh
REGISTRY_PROVIDER=gitlab    ./image-push.sh
REGISTRY_PROVIDER=bizfly    ./image-push.sh

REGISTRY_PROVIDER=all       ./image-push.sh
'

[ -z $REGISTRY_PROVIDER ] && (echo 'Envvar REGISTRY_PROVIDER is required' ; kill $$ ; exit 1)
[ ! -f "$SH/image-push.env" ] && ( printf "Input param file not found at image-push.env\nPlease clone one from image-push.env.tpl\n" ; kill $$ ; exit 1)

run() {
    [ ! -f "$SH/image-push--$REGISTRY_PROVIDER.sh" ] && (echo "Not found script for provider $REGISTRY_PROVIDER" ; kill $$ ; exit 1)
        source "$SH/image-push--$REGISTRY_PROVIDER.sh"
}

    if [ "$REGISTRY_PROVIDER" != 'all' ]; then
        run $REGISTRY_PROVIDER
    else  # run all
        run 'dockerhub'
        run 'gitlab'
        run 'bizfly'
    fi