#!/bin/bash
SH=$(cd $(dirname $BASH_SOURCE) && pwd)
AH=$(cd "$SH/../.." && pwd)

source "$SH/.env"
ln -sfn "$AH/.env" "$SH/.env.at-runtime"  # app's .env file is container runtime envvar

docker network rm     $DOCKER_NETWORK
docker network create $DOCKER_NETWORK

    [ -z "$VAR" ] && (echo 'Envvar $VAR is required'; kill $$; exit 1)

    #NOTE we customize app here by passing container-runtime envvar $VAR
    docker run \
        `# -e VAR="$VAR"  # envvar at runtime for container ref. https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file` \
        --env-file "$SH/.env.at-runtime"  `# envvar as file ref. docker run --help`  \
        --name $CONTAINER_NAME -d  -p $PORT:80 --network=$DOCKER_NETWORK $IMAGE_NAME
        #      c                   .  p                  n               i

        echo
        docker ps | grep $CONTAINER_NAME --color=always
