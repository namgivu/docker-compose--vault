#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

docstring='
ref.
https://docs.bizflycloud.vn/container_registry/howtos/
'

source "$SH/image-push.env"
    [ -z $BIZFLY_TOKEN_USER ] && (echo 'Param BIZFLY_TOKEN_USER is required'; kill $$; exit 1)
    [ -z $BIZFLY_TOKEN_KEY  ] && (echo 'Param BIZFLY_TOKEN_KEY is required' ; kill $$; exit 1)

    source "$SH/.env"  # load IMAGE_NAME

    echo "$BIZFLY_TOKEN_KEY" | docker login $BIZFLY_REGISTRY_HOST -u $BIZFLY_TOKEN_USER  --password-stdin  # login bizfly container registry
    #     !                                 .                     .                      .
    #     ref. https://docs.bizflycloud.vn/container_registry/howtos/authentication_and_authorization_using_token

        [ $? != 0 ] && (echo 'Login failed'; kill $$; exit 1)

            # image push ref. https://docs.bizflycloud.vn/container_registry/howtos/push_and_pull
            docker tag  $IMAGE_NAME "$BIZFLY_REPO_URI/$IMAGE_NAME"
            docker push             "$BIZFLY_REPO_URI/$IMAGE_NAME"
            #      .                =REMOTE_IMAGE_NAME
