#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

source "$SH/image-push.env"
    [ -z $GITLAB_TOKEN_USER ] && (echo 'Param GITLAB_TOKEN_USER is required' ; kill $$; exit 1)
    [ -z $GITLAB_TOKEN_KEY ]  && (echo 'Param GITLAB_TOKEN_KEY is required'  ; kill $$; exit 1)
    [ -z $GITLAB_REPO  ]      && (echo 'Param GITLAB_REPO is required'       ; kill $$; exit 1)
    [ -z $GITLAB_NAMESPACE ]  && (echo 'Param GITLAB_NAMESPACE is required'  ; kill $$; exit 1)

    source "$SH/.env"  # load IMAGE_NAME and GITLAB_xx params

    # login method1: docker login $GITLAB_REGISTRY_HOST -u $GITLAB_TOKEN_USER  -p $GITLAB_TOKEN_KEY  # login gitlab container registry
    #                             !                     .                .

    # login method2:
    echo "$GITLAB_TOKEN_KEY" | docker login $GITLAB_REGISTRY_HOST -u $GITLAB_TOKEN_USER  --password-stdin  # login gitlab container registry
    #     !                                 .                     .                      .

        [ $? != 0 ] && (echo 'Login failed'; kill $$; exit 1)

            #                       .
            #      .    .           .                      token' repo owner !must be token's repo
            docker tag  $IMAGE_NAME "$GITLAB_REGISTRY_HOST/$GITLAB_NAMESPACE/docker-compose--vault/$IMAGE_NAME"
            docker push             "$GITLAB_REGISTRY_HOST/$GITLAB_NAMESPACE/docker-compose--vault/$IMAGE_NAME"
            #      .                =REMOTE_IMAGE_NAME
