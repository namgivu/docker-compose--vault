#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

[ -z "$VAR" ] && (echo 'Envvar $VAR is required'; kill $$; exit 1)

eval "echo \"$(cat index.tpl.html)\"  > $SH/index.html"
#              =index.html with $VAR    .
#    =echo to fill value to $VAR

    [ -z $OVERWRITE_NGINX ] && (cat $SH/index.html; kill $$; exit)  # specified that NOT override nginx means printing result index.html only

    # set to nginx default serving homepage
    cp index.html /usr/share/nginx/html
