#!/bin/bash
# ref.
# https://stackoverflow.com/questions/37448357/ensure-kubernetes-deployment-has-completed-and-all-pods-are-updated-and-availabl
NS=default # namespace
[ -z $l  ] && (echo 'Envvar :c is required'  ; kill $$; exit 1)     # l aka label of pod
[ -z $kw ] && (echo 'Envvar :kw is required' ; kill $$; exit 1)  # kw aka keyword to wait for in pod's envvar runtime

    echo; printf "Wait until the pods ready ..."
        while true; do
            # get pod name of label $l
            pod=$(kubectl -n $NS  get pods -l=$l --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
                # seek for configmap key :kw in $pod 's envvar ref. https://stackoverflow.com/questions/59816462/how-to-verify-the-rolling-update
               found=$(kubectl -n $NS exec $pod -- env 2>&1 | grep -c "$kw" )
                #               .    !    !
                    if [ "$found" == "1" ]; then
                        break
                    fi

            printf '.'; sleep 1;
        done
