#!/bin/bash
CONTAINER_NAME=$1  		# name container's volume you want backup
DIR_INTO_VOLUME=$2 		# dir you want backup
OUTPUT_TAR_DIR=$3  		# palce you save file .tar
TIME=`date +%Y%m%d_%H%M%S`
NAME="backup_$TIME.tar"

usage() {
  echo "Usage: $0 [container name] [dir you want backup] [place save file ]"
  exit 1
}

if [ -z $CONTAINER_NAME ]
then
  echo "Error: missing container name parameter."
  usage
fi

if [ -z $DIR_INTO_VOLUME ]
then
  echo "Error: missing volume name parameter."
  usage
fi

if [ -z $OUTPUT_TAR_DIR ]
then
  echo "Error: missing output dir name parameter."
  usage
fi

docker run --rm --volumes-from $CONTAINER_NAME -v $OUTPUT_TAR_DIR:/backup busybox tar cvf /backup/$NAME $DIR_INTO_VOLUME

echo y | docker image prune  >/dev/null 2>&1

cd $OUTPUT_TAR_DIR 
    
if [ -f $NAME ]
then

	if ! { tar tf "$NAME" ;} >/dev/null 2>&1
	then
    echo "backup fail"
    else
    	echo "done"
	fi

else
   echo "backup fail"
fi