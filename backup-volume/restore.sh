#!/bin/bash
CONTAINER_NAME_RESTORE=$1
INPUT_TAR_DIR=$2  # palce contain file backup
FILE_BACKUP=$3	 # file .tar

usage() {
  echo "Usage: $0 [container name] [dir you save file .tar] [file .tar]"
  exit 1
}

usage_() {
  echo "Usage: $0 only support file .tar"
  exit 1
}

if [ -z $CONTAINER_NAME_RESTORE ] ; then
  echo "Error: missing container name parameter."
  usage
fi

if [ -z $INPUT_TAR_DIR ] ; then
  echo "Error: missing dir name parameter."
  usage
fi

if [ -z $FILE_BACKUP ] ; then
  echo "Error: missing file name parameter."
  usage
fi

cd $INPUT_TAR_DIR

if [ -f $FILE_BACKUP ]
then

	if ! { tar tf "$FILE_BACKUP"; } >/dev/null 2>&1; then
    echo "$FILE_BACKUP is illegal"
    usage_
	fi

else
   echo "file does not exist" ; exit 1 ;
fi

docker run --rm --volumes-from $CONTAINER_NAME_RESTORE -v $INPUT_TAR_DIR:/backup busybox tar xvf /backup/$FILE_BACKUP

echo y | docker image prune  >/dev/null 2>&1


