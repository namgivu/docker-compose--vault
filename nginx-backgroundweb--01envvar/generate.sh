#!/usr/bin/env bash
SH=$(cd $(dirname $BASH_SOURCE) && pwd)

env_f="$SH/.env"; if [ -f $env_f ]; then source $env_f; fi  # load $VAR if any

echo "[DEBUG] VAR=$VAR loaded from envvar"

    [ -z $VAR ] && (echo 'Envvar $VAR is required'; kill $$; exit 1)
    eval "echo \"$(cat index.tpl.html)\" > index.html"
