#!/bin/bash
SH=$(cd $(dirname $BASH_SOURCE) && pwd)

source "$SH/.env"

docker network rm     $DOCKER_NETWORK
docker network create $DOCKER_NETWORK

    #TODO pass container-runtime envvar to customize app
    docker run --name $CONTAINER_NAME -d  -p $PORT:80 --network=$DOCKER_NETWORK $IMAGE_TAG
    #      .          c                   .  p                  n               i
        echo
        docker ps | grep $CONTAINER_NAME --color=always
            [ $? == 0 ] && echo 'pass' || echo 'fail'
