#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/.." && pwd)

(cd $AH && docker-compose up -d)  #NOTE this will auto-load .env file into build-time var in docker-compose.yml

    source "$AH/.env"
    c=$CONTAINER_WP kw='resuming normal operations'  source "$SH/wait4ready.sh"
    # .                 .                                        !

    echo
    source "$SH/aftermath.sh"
