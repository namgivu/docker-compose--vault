#!/bin/bash
if [ -z "$c"  ]; then echo "Envvar :c is required"  ; exit 1; fi  # c aka container
if [ -z "$kw" ]; then echo "Envvar :kw is required" ; exit 1; fi  # kw aka keyword in log

    echo; printf "Wait until the apps ready ..."
        while true; do
            #                   !                  !
            found=$(docker logs $c 2>&1 | grep -c "$kw")
            #                      ! must have this 2>&1 to properly grep from docker logs ref. https://stackoverflow.com/a/38207098/248616
                if [ "$found" == "1" ]; then
                    break
                fi
                printf '.'; sleep 1;

            # echo "--- DEBUG $c $kw $found"
        done
        echo ' Done'
