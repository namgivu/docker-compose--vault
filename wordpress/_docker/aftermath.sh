#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

AH=$(cd "$SH/.." && pwd)
source "$AH/.env"

    echo ; echo 'Check if containers created ...'
        docker ps --format '{{.Names}} {{.Ports}}'  | grep -E "wp|db|$PORT_WP|$PORT_DB" --color=always
        if [ "$?" != "0" ]; then echo 'ERROR docker-compose up failed'; exit 1; fi

    echo ; echo "Check if wordpress webapp ready at http://localhost:$PORT_WP ..."
        #                          !testee                        !expected_output                                                  .error message
        curl -sI "http://localhost:$PORT_WP" | head -n1 | grep -c 'HTTP/1.1 302 Found' 1>&/dev/null ; if [ "$?" != "0" ]; then echo 'ERROR Must be 302 as redirected'; exit 1; fi
        curl -sI "http://localhost:$PORT_WP"            | grep -c 'X-Redirect-By:'     1>&/dev/null ; if [ "$?" != "0" ]; then echo 'ERROR Must be redirected to /wp-admin'; exit 1; fi

        #                                                                                      . remove CR ref. https://stackoverflow.com/a/15521258/248616
        wp_admin_url=$(curl -sI "http://localhost:$PORT_WP" | grep 'Location' | cut -d' ' -f2 | tr -d '\r')
        curl -sI $wp_admin_url | head -n1 | grep -c '200 OK' 1>&/dev/null ; if [ "$?" != "0" ]; then echo 'ERROR Cannot open /wp-admin'; exit 1; fi

echo
echo 'Everything looks good!'
