#!/bin/bash
# ref.
# https://stackoverflow.com/questions/37448357/ensure-kubernetes-deployment-has-completed-and-all-pods-are-updated-and-availabl

if [ -z $l  ]; then echo "Envvar :c is required"  ; exit 1; fi  # l aka label of pod
if [ -z $kw ]; then echo "Envvar :kw is required" ; exit 1; fi  # kw aka keyword to wait for in pod's envvar

    echo; printf "Wait until the pods ready ..."
        while true; do
            # get pod name of label $l
            pod=$(kubectl get pods -l=$l --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
                # seek for configmap key :kw in $pod 's envvar ref. https://stackoverflow.com/questions/59816462/how-to-verify-the-rolling-update
               found=$(kubectl exec $pod -- env 2>&1 | grep -c "$kw" )
                #               .    !    !
                    if [ "$found" == "1" ]; then
                        break
                    fi

            printf '.'; sleep 1;
        done
