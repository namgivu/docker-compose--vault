Research [here](https://github.com/groovemonkey/project-based-kubernetes/blob/master/projects/wordpress/Project-Instructions.md)

### Infrastructure Diagram:

    Some config data            *(k8s ConfigMaps and Secrets)*

    MySQL Container             *(k8s replicaset)*
    MySQL Service               *(k8s service)*
        |
        |
    WordPress Container         *(k8s deployment)*
    [ apache and php-fpm ]
        |
        |
        |
    GKE Loadbalancer            *(k8s service)*


## 1. Mysql Setup

Create your mysql volume claim, replicaset, service, and configmap - docker image mysql:latest
    
```
kubectl apply -f manifests/mysql-configmap.yml
kubectl apply -f manifests/mysql-service.yml
kubectl apply -f manifests/mysql-volume-claim.yml
kubectl apply -f manifests/mysql-replicaset.yml
```
    

## 2. Wordpress Setup

Create your wordpress volume and deployment with docker image wordpress:latest                                                                                                                                                                                   
```
kubectl apply -f manifests/wordpress-configmap.yml
kubectl apply -f manifests/wordpress-service.yml
kubectl apply -f manifests/wordpress-datavolume-claim.yml
kubectl apply -f manifests/wordpress-deployment.yml
```
## 3. Setup gitlab secret

Create token
go to GPC console and see details in this [Link] (https://medium.com/google-cloud/gitlab-continuous-deployment-pipeline-to-gke-with-helm-69d8a15ed910)

Cerate secret
As always, you access the cluster 
```
gcloud container clusters get-credentials cluster-free-300 --zone us-central1-c --project esoteric-storm-295002
```
and now, you can apply secret
```
gcloud container clusters get-credentials $GCP_CLUSTER --zone $GCP_ZONE --project $GCP_PROJECT_ID