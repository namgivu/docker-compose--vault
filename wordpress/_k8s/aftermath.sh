#!/usr/bin/env bash
 NS=default  # NS aka namespace
SVC=wordpress-loadbalance  # SVC aka service

echo
printf "Wait until the loadbalancer ready ..."
    while true; do
        LoadBalancer=$(kubectl -n $NS get svc $SVC -o jsonpath='{..ip}')
        if [ ! -z "$LoadBalancer" ]; then break; fi
        printf '.'; sleep 1;
    done

echo
echo "Your service/loadbalancer is ready at $loadbalancer"
    #TODO this step is same as _docker/aftermath.sh > Check if wordpress webapp ready at http://localhost:$PORT_WP
    curl -sI "http://$LoadBalancer" | head -n1 | grep -c 'HTTP/1.1 302 Found' 1>&/dev/null ; has_error=$?
    if [ "$has_error" != "0" ]; then
        echo 'ERROR wordpress not working';
    else
        echo 'Everything looks good!'
    fi
